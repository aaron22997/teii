# -*- coding: utf-8 -*-ç
import cv2
import sys
import numpy as np
import matplotlib.pyplot as plt
import ctypes as ct
from time import time




def show_plot_and_wait_for_key():
    """
        Con esta funcion vamos a conseguir poder dibujar
        todos los histogramas, ademas nos va a permitir 
        poder calcular de forma correcta todos los 
        tiempos de ejecucion.
    """
    plt.draw()
    plt.pause(0.01)
    input("<Hit Enter To Close>")
    plt.close()

# FUNCION PARA CALCULAR EL HISTOGRAMA MEDIANTE FUNCION C
def graficar_C(foto):
    
    data = np.asarray(foto)#Creamos el array con los datos de la foto
    
    array = data.ravel() #Pasamos a array de una dimensión   

    height = img.shape[0]#Calculamos la altura 
    width = img.shape[1]#Calculamos la anchura de la foto
        
    hist = np.zeros(256, dtype=int)#Creamos un array de 256 posiciones 
           
    #Cargamos la librería de C
    libsimple = np.ctypeslib.load_library('/home/alumno/Escritorio/histograma/libhistograma.so', './')

    #Especificacion de  tipos  de los  parametros  de  entrada:
    libsimple.histogramEqualisation.argtypes = \
    [ct.c_int,
     ct.c_int,
     np.ctypeslib.ndpointer(dtype=np.uint8 , ndim = 1 , flags ='C'),
     np.ctypeslib.ndpointer(dtype=np.int64, ndim = 1 , flags ='C')]
    #Llamada a la función de la librería
    libsimple.histogramEqualisation(width, height, array, hist)    
    
    fig = plt.figure() #Creamos la figura en la que vamos a representar los datos 
    ax = fig.add_subplot(111) #Establecemos los ejes 
    plt.title("Histograma con C " + str(height) + "x" +  str(width))
    xx = range(len(hist))#Establecemos el numero de datos 
    
    ax.bar(xx, hist, width=1, align='center')#Introducimos los datos a la figura 
    
    plt.show()#Dibujamos
    
    return None

    
    # FUNCION PARA CALCULAR EL HISTOGRAMA MEDIANTE CODIGO PYTHON PURO (USANDO BUCLES)
def graficar_bucles(foto):
    
    array_histograma = [0]*256 #Array en el que guardamos los valores de cada pixel
    height = foto.shape[0] #Altura de la imagen
    width = foto.shape[1] #Anchura de la imagen
    
  
    """
        A traves de este doble bucle anindado lo que vamos a conseguir 
        es ir recorriendo todos los pixeles de la imagen e ir guardando
        su valor.
    """
    for i in range(height):
        for j in range(width):
            aux = foto[i,j]
            array_histograma[aux] = array_histograma[aux] + 1 
            
    
       
    """
        Ahora vamos a crear el histograma a partir del array anterior,
        indicando ademas el número de el numero de bins, asi como, 
        el rango que vamos a utilizar. 
        Posteriormente establecemos el nombre del histograma, indicando
        el alto y el ancho.
    """
    fig = plt.figure() #Creamos la figura en la que vamos a representar los datos 
    ax = fig.add_subplot(111) ##Establecemos los ejes 
    plt.title("Histograma con bucles " + str(height) + "x" +  str(width))
    xx = range(len(array_histograma))#Establecemos el numero de datos 
        
    ax.bar(xx, array_histograma, width=1, align='center')#Introducimos los datos a la figura 
    
    plt.show()#Dibujamos

    return None


    # FUNCION PARA CALCULAR EL HISTOGRAMA MEDIANTE NUMPY
def graficar_numPy(foto):
    
    hist,bins = np.histogram(foto,256,[0,256])
    
    height = str(foto.shape[0]) #Altura de la imagen
    width = str(foto.shape[1]) #Anchura de la imagen 
    """
        Ahora vamos a crear el histograma a partir del array anterior,
        indicando además el número de el número de bins, así como, 
        el rango que vamos a utilizar. 
        Posteriormente establecemos el nombre del histograma, indicando
        el alto y el ancho.
    """
    plt.hist(foto.ravel(),256,[0,256])
    plt.title("Histograma con numPy " + height + "x" + width)
    
    return None

    #FUNCION PARA MOSTRAR LOS DIAGRAMAS DE LOS TIEMPOS DE EJECUCIÓN DE CADA METODO POR SEPARADO
def show_result(array_tiempos, var, foto, numero):
    
    height = foto.shape[0] #Altura de la imagen
    width = foto.shape[1] #Anchura de la imagen
    nombres = [] #Array en el que almacenaremos los tamaños de las imagenes que aparecerán en el eje horizontal
    
    """
        Con este bucle, vamos a ir calculando para cada iteracion el 
        el tamaño de la imagen en cada una de las escalas, y este
        tamaño lo introduciremos en el array que posteriormente usaremos 
        para indicar estos tamaños en el eje X.
    """
    for i in range(1,numero+1):

        b = str(height * i)
        a = str(width * i)
        c = a + "x" + b 
        nombres.append(c)
    
    
    fig = plt.figure(u'Gráfica de barras') # Creamos la figura
    ax = fig.add_subplot(111) # Establecemos los ejes 
 
    xx = range(len(array_tiempos))
    plt.title("Histograma de tiempos usando "+ var) #Establecemos el título
    ax.bar(xx, array_tiempos,width=0.8, align='center') #Añadimos los valores a los ejes
    ax.set_xticks(xx) #Asignamos el rango a los ejes
    ax.set_xticklabels(nombres) #Colocamos los tamaños al eje X que hemos calculado antes.
    
    plt.show() #Dibujamos la figura
    
    fig.savefig(var + ".svg")
    
    return None


     #FUNCION PARA MOSTRAR EL DIGRAMA DE LOS TIEMPOS DE EJECUCION DE LOS TRES METODOS JUNTOS.
def show_result_2(array_tiempos, foto, numero):
    
    height = foto.shape[0] #Altura de la imagen
    width = foto.shape[1] #Anchura de la imagen
    nombres = [] #Array en el que almacenaremos los tamaños de las imagenes que apareceran en el eje horizontal
    """
        Con este bucle, vamos a ir calculando para cada iteracion el 
        el tamaño de la imagen en cada una de las escalas, y este
        tamaño lo introduciremos en el array que posteriormente usaremos 
        para indicar estos tamaños en el eje X.
        En este caso, como vamos a tener 3 barras para cada tamaño de la imagen, 
        lo que vamos a hacer es poner una barra "-" para no triplicar el texto
        que va a salir en el eje horizontal
    """
    
    for i in range(1,numero+1):
        b = str(height * i)
        a = str(width * i)
        c = a + "x" + b 
        nombres.append("-")
        nombres.append(c)
        nombres.append("-")
        
    
    fig = plt.figure(u'Gráfica de barras') # Creamos la figura
    ax = fig.add_subplot(111) # Establecemos los ejes 
    
    
    xx = range(len(array_tiempos))
    plt.title("Histograma de tiempos usando los tres metodos ") #Establecemos el título
    ax.bar(xx, array_tiempos,width=0.8, align='center') #Añadimos los valores a los ejes
    ax.set_xticks(xx) #Asignamos el rango a los ejes
    ax.set_xticklabels(nombres) #Añadimos los tamños al eje X que hemos calculado antes.
    
    plt.show() #Dibujamos la figura
    fig.savefig("resultados.svg")
    return None

    #CREAMOS EL PROGRAMA 
if __name__ == '__main__':   

    #Comprobamos que el numero de argumentos que introducimos por linea de comandos sea el correcto
    if len(sys.argv) != 3:
        print("El numero de parametros es incorrecto")
        print("Uso: \"ruta\" numero")
        sys.exit(0)
    #Comprobamos que el número que introducimos sea correcto:
    try:
        N = float(sys.argv[2])
        if not (1.0 <= N <= 10.0):
            raise ValueError()
    except:
        print("N must be a float value between 1.0 and 10.0")
        sys.exit(-1)

  
    numero=int(sys.argv[2]) #Guardamos el valor del numero inroducido por linea de comandos
    ruta=sys.argv[1]#Guardamos la ubicación de la foto inroducida por linea de comandos
    
    
    
    foto_ = plt.imread(ruta)
    foto = cv2.cvtColor(foto_, cv2.COLOR_BGR2GRAY)
   
    """
        Creamos 4 arrays en los cuales vamos a ir almacenando los tiempos de 
        ejecicion, tanto de cada metodo por separado como de los 3 metodos 
        de forma conjunta
    """
    tiempos_ejecucion_bucles =[]
    tiempos_ejecucion_numPy =[]
    tiempos_ejecucion_C =[]
    tiempos_ejecucion_combinados = []
    
    #Comenzamos el bucle de ejecicion para las X escalas 
    for i in range(1,numero+1):

        height = int(foto.shape[1]) #Altura de la foto
        width = int(foto.shape[0])#Anchura de la foro
        
        #Reescalamos la anchura y la anchura
        width = width*i
        height = height*i
        img = cv2.resize(foto, (width, height)) #Reescalamos la foto
        
        #############################################
        #PYTHON PURO
        tiempo_ini = time()#Iniciamos el contador del tiempo de ejecución
        graficar_bucles(img) #Calculamos el histograma mediante Python puro
        tiempo_fin=time()#Finalizamos el contador del tiempo de ejeción
        
        tiempo = tiempo_fin - tiempo_ini #Calculamos el tiempo de ejecucion
        #Introducimos el tiempo de ejecucion en milisegundos en los dos arrays
        tiempos_ejecucion_bucles.append(tiempo*1000)
        tiempos_ejecucion_combinados.append(tiempo*1000)
        #Ahora mostramos el resultado del tiempo de ejecución
        print ('El tiempo de ejecucion mediante bucles fue:', '%.3f'%(tiempo*1000) + " milisegundos")
        
        show_plot_and_wait_for_key()#Función para continuar con la ejecución
        ###############################################
        #NUMPY
        tiempo_ini = time()#Iniciamos el contador del tiempo de ejecucion
        graficar_numPy(img)#Calculamos el histograma mediante NumPy
        tiempo_fin=time()#Finalizamos el contador del tiempo de ejeción
        
        tiempo = tiempo_fin - tiempo_ini#Calculamos el tiempo de ejecucion
        #Introducimos el tiempo de ejecucion en milisegundos en los dos arrays
        tiempos_ejecucion_numPy.append(tiempo*1000)
        tiempos_ejecucion_combinados.append(tiempo*1000)
        #Ahora mostramos el resultado del tiempo de ejecucion
        print ('El tiempo de ejecucion mediante numPy fue:', '%.3f'%(tiempo) + " milisegundos")
        
        show_plot_and_wait_for_key()#Funcion para continuar con la ejecucion
        ###############################################
        #LIBRERIAS C
        tiempo_ini = time()#Iniciamos el contador del tiempo de ejecución
        graficar_C(img) #Calculamos el histograma mediante Python puro
        tiempo_fin=time()#Finalizamos el contador del tiempo de ejeción
        
        tiempo = tiempo_fin - tiempo_ini #Calculamos el tiempo de ejecucion
        #Introducimos el tiempo de ejecucion en milisegundos en los dos arrays
        tiempos_ejecucion_C.append(tiempo*1000)
        tiempos_ejecucion_combinados.append(tiempo*1000)
        #Ahora mostramos el resultado del tiempo de ejecución
        print ('El tiempo de ejecucion mediante bucles fue:', '%.3f'%(tiempo*1000) + " milisegundos")
        
        show_plot_and_wait_for_key()#Función para continuar con la ejecución
        
        #############################################
    """
        Ahora, una vez finalizada la ejecucion, vamos a mostras las diferentes
        comparativas que hay de los tiempos:
    """
    #Tiempos de ejecucion mediante PYTHON
    show_result(tiempos_ejecucion_bucles, "bucles", foto, numero)
    
    #Tiempos de ejecucion mediante Numpy
    show_result(tiempos_ejecucion_numPy, "numPy", foto, numero)
    
    #Tiempos de ejecucion mediante C
    show_result(tiempos_ejecucion_C, "C", foto, numero)
   
    #Tiempos de ejecucion completos 
    show_result_2(tiempos_ejecucion_combinados, foto, numero)
    
    
   


