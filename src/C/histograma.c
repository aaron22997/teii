
  
// Incluimos todas las librerias necesarias para este programa
#include <fcntl.h> 
#include <math.h> 
#include <stdio.h> 
#include <stdlib.h> 
#include <string.h>
#include "histograma.h"
  
/*
*Funcion para realizar calcular el histograma de una imagen.
*La funcion toma el total de filas, columnas, nombre de archivo de entrada y salida
*nombre de archivo como parametros
*/
void histogramEqualisation(int width, int height, unsigned char* img, long long* hist) 
{ 
    int n = width*height;
    for(int i = 0; i < n; i++)
		hist[img[i]]++;
} 
  

int main() 
{ 
    return 0; 
}
